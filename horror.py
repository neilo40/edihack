import pyglet
pyglet.resource.path = ["assets"]
pyglet.resource.reindex()

WIDTH = 800

window = pyglet.window.Window(WIDTH, 600)
sky_sprite = pyglet.sprite.Sprite(pyglet.resource.image("sky.png"))
cloud_sprite = pyglet.sprite.Sprite(pyglet.resource.image("cloud.png"))
grass_sprite = pyglet.sprite.Sprite(pyglet.resource.image("grass.png"))
hills_sprite = pyglet.sprite.Sprite(pyglet.resource.image("hills.png"))
tree_sprite = pyglet.sprite.Sprite(pyglet.resource.image("tree.png"))
car_sprite = pyglet.sprite.Sprite(pyglet.resource.image("car.png"))

positions = {"sky": [0, 200], "hills": [0, 200], "cloud": [250, 450],
             "grass": [0, 0], "car": [300, 200], "tree": [200, 0]}

joysticks = pyglet.input.get_joysticks()
joystick = joysticks[0]
joystick.open()


def move():
    step = 10
    if positions["hills"][0] <= -WIDTH:
        positions["hills"][0] += WIDTH
    positions["hills"][0] -= (step * 0.5)

    if positions["cloud"][0] <= -400:
        positions["cloud"][0] += 400
    positions["cloud"][0] -= (step * 0.75)

    if positions["grass"][0] <= -WIDTH:
        positions["grass"][0] += WIDTH
    positions["grass"][0] -= (step * 1)

    if positions["tree"][0] <= -400:
        positions["tree"][0] += 400
    positions["tree"][0] -= (step * 1.5)


def update(dt):
    if joystick.hat_x > 0.5:
        move()


pyglet.clock.schedule_interval(update, 1/60.0)


@window.event
def on_draw():
    window.clear()
    sky_sprite.set_position(*positions["sky"])
    sky_sprite.draw()

    hills_sprite.set_position(*positions["hills"])
    hills_sprite.draw()
    hills_sprite.set_position(positions["hills"][0]+WIDTH, positions["hills"][1])
    hills_sprite.draw()

    cloud_sprite.set_position(*positions["cloud"])
    cloud_sprite.draw()
    cloud_sprite.set_position(positions["cloud"][0]+400, positions["cloud"][1])
    cloud_sprite.draw()

    grass_sprite.set_position(*positions["grass"])
    grass_sprite.draw()
    grass_sprite.set_position(positions["grass"][0]+WIDTH, positions["grass"][1])
    grass_sprite.draw()

    car_sprite.set_position(*positions["car"])
    car_sprite.draw()

    tree_sprite.set_position(*positions["tree"])
    tree_sprite.draw()
    tree_sprite.set_position(positions["tree"][0]+300, positions["tree"][1])
    tree_sprite.draw()
    tree_sprite.set_position(positions["tree"][0]+500, positions["tree"][1])
    tree_sprite.draw()


if __name__ == "__main__":
    pyglet.app.run()
